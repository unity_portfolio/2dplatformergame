using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("# 플레이어 이동 관련")]
    /// <summary>
    /// 이동 속력
    /// </summary>
    public float m_Speed;

    /// <summary>
    /// 점프 힘
    /// </summary>
    public float m_JumpPower;

    /// <summary>
    /// 중력에 적용될 승수
    /// </summary>
    public float m_GravityMultiplier;

    /// <summary>
    /// 충돌 감지할 레이어
    /// </summary>
    public LayerMask m_LayerMask;

    /// <summary>
    /// 현재 속도
    /// </summary>
    private Vector3 _Velocity;

    /// <summary>
    /// 입력 축 값
    /// </summary>
    private Vector3 _InputVelocity;

    /// <summary>
    /// 캐릭터 영역
    /// </summary>
    private BoxCollider2D _BoxCollider;

    /// <summary>
    /// 점프 입력이 들어왔음을 나타냄
    /// </summary>
    private bool _IsJump;

    /// <summary>
    /// 땅에 닿아있음을 나타냄
    /// </summary>
    private bool _IsGrounded;

    public BoxCollider2D boxCollider => _BoxCollider ?? (_BoxCollider = GetComponent<BoxCollider2D>());

    /// <summary>
    /// 현재 속도의 읽기 전용 프로퍼티
    /// </summary>
    public Vector3 velocity => _Velocity;

    public Vector3 inputVelocity => _InputVelocity;

    public bool isGround => _IsGrounded;


    private void FixedUpdate()
    {
        Vector2 origin = transform.position;

        Vector2 halfsize = boxCollider.size * 0.5f;

        // X 축 속도 계산
        CalculateVelocityX(origin,halfsize);

        // Y 축 속도 계산
        CalculateVelocityY(origin, halfsize);

        // 계산된 속도로 이동
        transform.position += _Velocity;
    }

    /// <summary>
    /// X 축 속도를 계산
    /// </summary>
    private void CalculateVelocityX(Vector2 origin, Vector2 halfsize)
    {
        float velocityX = _Velocity.x;

        // 입력 값을 속도로 변환
        velocityX = m_Speed * _InputVelocity.x * Time.fixedDeltaTime;

        Vector2 direction = Vector2.right * Mathf.Sign(_InputVelocity.x);

        float maxDistance = halfsize.x + Mathf.Abs(velocityX);

        int layerMask = m_LayerMask;

        RaycastHit2D hit = CheckCollision(origin, direction, maxDistance, layerMask);

        bool isHit = (hit.collider != null);

        if (isHit)
        {
            float distance = hit.distance - halfsize.x;

            // 벽을 감지한 경우 벽까지 이동하며, 벽을 통과하지 않도록 합니다.
            int sign = direction.x > 0.0f ? 1 : -1;
            velocityX = distance * sign;
        }

        _Velocity.x = velocityX;
    }

    /// <summary>
    /// Y 축 속도를 계산
    /// </summary>
    private void CalculateVelocityY(Vector2 origin, Vector2 halfsize)
    {
        _IsGrounded = false;

        float gravity = Mathf.Abs(Physics2D.gravity.y) * m_GravityMultiplier * Time.fixedDeltaTime;
        float velocityY = _Velocity.y;

        // 중력 연산
        velocityY -= gravity;

        if (_IsJump)
        {
            _IsJump = false;
            velocityY = m_JumpPower;
        }

        // 방향
        Vector2 direction = Vector2.up * Mathf.Sign(velocityY);

        // 검사 길이
        float maxDistance = halfsize.y + Mathf.Abs(velocityY);

        int layerMask = m_LayerMask;

        RaycastHit2D hit = CheckCollision(origin, direction, maxDistance, layerMask);

        bool isHit = (hit.collider != null);

        if (isHit)
        {
            if (velocityY < 0.0f) _IsGrounded = true;

            float distance = hit.distance - halfsize.y;
            velocityY = distance * direction.y;
        }

        _Velocity.y = velocityY;

    }

    /// <summary>
    /// 충돌체를 감지합니다.
    /// </summary>
    /// <param name="origin">충돌을 감지할 오브젝트</param>
    /// <param name="direction">방향</param>
    /// <param name="distance">충돌 감지 거리</param>
    /// <param name="layerMask">충돌체 레이어</param>
    /// <returns>충돌체 자체를 반환</returns>
    private RaycastHit2D CheckCollision(Vector2 origin, Vector2 direction, float distance, int layerMask)
    {
        RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layerMask);

        return hit;
    }

    /// <summary>
    /// 입력 값 중 X 축 값을 전달합니다.
    /// </summary>
    /// <param name="axisValue"></param>
    public void OnMovementInput(Vector2 axisValue)
    {
        _InputVelocity.x = axisValue.x;
    }

    public void OnJumpInput()
    {
        if (!_IsJump && _IsGrounded)
        {
            _IsJump = true;
        }
    }


}
