using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimControll : MonoBehaviour
{
    private Animator _Animator;

    private SpriteRenderer _Renderer;

    private const string PARAMNAME_SPEED = "_Speed";

    private const string PARAMNAME_ISGROUND = "_IsGround";

    private Vector3 _MoveSpeed;

    private bool _CurrentFlipX;

    public Vector3 currentVelocity { get; set; }

    public bool isGround { get; set; }

    public bool currentFlipX => _CurrentFlipX;

    private void Awake()
    {
        _Animator = GetComponent<Animator>();
        _Renderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        _MoveSpeed = Vector3.MoveTowards(_MoveSpeed, currentVelocity, 20.0f * Time.deltaTime);

        UpdateSpeed(_MoveSpeed);

        UpdateGrounded();

        SpriteFlipX();
    }

    private void UpdateSpeed(Vector3 speed)
    {
        float speedLength = speed.magnitude;
        _Animator.SetFloat(PARAMNAME_SPEED, speedLength);
    }

    private void UpdateGrounded()
    {
        _Animator.SetBool(PARAMNAME_ISGROUND,isGround);
    }

    private void SpriteFlipX()
    {

        if(currentVelocity.x > 0)
        {
            _Renderer.flipX = false;
        }
        else if(currentVelocity.x < 0) 
        {
            _Renderer.flipX = true;
        }
        _CurrentFlipX = _Renderer.flipX;
    }


}
