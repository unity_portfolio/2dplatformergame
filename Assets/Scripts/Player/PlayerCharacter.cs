using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    /// <summary>
    /// 이동 기능을 담당하는 컴포넌트입니다.
    /// </summary>
    private PlayerMovement _MovementComponent;

    /// <summary>
    /// 이 캐릭터를 조종하고 있는 플레이어 컨트롤러를 나타냅니다.
    /// </summary>
    private PlayerController _PlayerController;

    private PlayerAnimControll _PlayerAnimControll;

    /// <summary>
    /// 총알 발사대 컴포넌트
    /// </summary>
    private BulletGenerator _BulletGenerator;

    /// <summary>
    /// 플레이어 컨트롤러에게 조종당하고 있음을 나타냅니다.
    /// </summary>
    public bool isControlled => _PlayerController != null;

    private void Awake()
    {
        _MovementComponent = GetComponent<PlayerMovement>();
        _BulletGenerator = GetComponentInChildren<BulletGenerator>();
        _PlayerAnimControll = GetComponentInChildren<PlayerAnimControll>();
    }

    private void Update()
    {
        AnimParamUpdate();
    }

    private void AnimParamUpdate()
    {
        // 현재 속력
        float velocityLength = _MovementComponent.velocity.magnitude;

        // 입력 축을 얻습니다.
        Vector3 animationAxis = new Vector3(_MovementComponent.inputVelocity.x, 0, 0);

        _PlayerAnimControll.currentVelocity = velocityLength * animationAxis;

        _PlayerAnimControll.isGround = _MovementComponent.isGround;
    }

    /// <summary>
    /// 이 캐릭터의 조종이 시작될 때 호출됩니다.
    /// </summary>
    /// <param name="playerController">조종을 시작하는 플레이어 컨트롤러 객체가 전달됩니다.</param>
    public virtual void OnControlStarted(PlayerController playerController)
    {
        _PlayerController = playerController;
    }

    /// <summary>
    /// 이 캐릭터의 조종이 끝났을 때 호출됩니다.
    /// </summary>
    public virtual void OnControlFinished()
    {
        _PlayerController = null;
    }

    public void OnMovementInput(Vector2 axisValue) => _MovementComponent.OnMovementInput(axisValue);
    public void OnJumpInput() => _MovementComponent.OnJumpInput();

    public void OnAttackInput()
    {
        _BulletGenerator.GenerateBullet();
    }

}
