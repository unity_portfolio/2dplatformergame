using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [Header("# �Ѿ� �ӷ�")]
    public float m_Speed;

    private float sign;

    private Vector3 _StartPosition;

    private PlayerMovement _PlayerMovement;

    private PlayerAnimControll _PlayerAnimControl;

    private void Awake()
    {
        _PlayerMovement = FindObjectOfType<PlayerMovement>();
        _PlayerAnimControl = FindObjectOfType<PlayerAnimControll>();
    }

    private void Start()
    {
        if(_PlayerMovement.velocity.x != 0.0f)
        { 
            sign = Mathf.Sign(_PlayerMovement.velocity.x);
        }
        else
        {
            sign = (_PlayerAnimControl.currentFlipX) ? -1.0f : 1.0f; 
        }

        _StartPosition = transform.position;
    }

    private void FixedUpdate()
    {
        FlyingBullet();

        DestroyBullet();
    }

    private void FlyingBullet()
    {
        Vector2 currentPosition = transform.position;

        currentPosition.x += sign * m_Speed * Time.fixedDeltaTime;

        transform.position = currentPosition;
    }

    private void DestroyBullet()
    {
        float distance = Mathf.Abs(transform.position.x - _StartPosition.x);

        if (distance >= 6.0f)
        {
            Destroy(gameObject);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Player")
        {
            Destroy(gameObject);
        }
    }
}
